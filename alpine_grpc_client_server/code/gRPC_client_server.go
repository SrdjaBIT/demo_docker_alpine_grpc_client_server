package main

import (
	"os"
	//"fmt"
	"do_remote"
	"log"
	"math/rand"
	"net"
	"strings"
	"time"

	pb "./protos"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port        = ":50051"
	user        = "root"
	root        = "root"
	hostname    = "172.17.0.4"
	password    = "root"
	address     = "172.17.0.4:50051"
	defaultName = "grpc_client_server"
)

// Class server is used to implement test_service.TestService.
type server struct{}

// Format notification.
func notifyFormatter(notifStr string, notifType pb.Response_ResultType) []*pb.Response_Notification {
	return []*pb.Response_Notification{
		{
			Notifstr:   notifStr,
			Rtypenotif: notifType,
		},
	}
}

// Connect into root@172.17.0.4 and execute gRPC.
func doGrpc() {
	// Set up a client connection to the given target (server).
	// Dial from grpc package.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	d := pb.NewTestServiceClient(conn)

	// Check program parameters.
	name := defaultName
	tabs := "\t\t\t"

	// Generate two random numbers of Float64 type. Repeat this 10000 times.
	a := rand.Float64() * 100
	b := rand.Float64() * 100

	// Call DoSomething function on a remote machine 10000 times and print out its response.
	// DoSomething will multiply two floats and generate a notification based on result.
	start := time.Now()
	resp, err := d.DoSomething(context.Background(), &pb.Request{Name: name, A: a, B: b})
	if err != nil {
		log.Fatalf("could not request: %v", err)
	}
	elapsed := time.Since(start) // 2016/11/09 16:38:56 Elapsed: 135.331µs - is that ok?
	log.Println("+++++++++++++++++")
	//log.Printf("Call No: %d", i)
	log.Printf("Name: %s", resp.Name)
	log.Printf("Result: %.2f", resp.Result)
	log.Printf("Notif:\n%s%s\n%sValue: %s",
		tabs, resp.Notif[0].Notifstr, tabs, resp.Notif[0].Rtypenotif)
	log.Printf("Time elapsed: %s\n", elapsed)
}

// Check results and generate a notification.
func notifyByResult(res float64) []*pb.Response_Notification {
	if res < 2000 {
		return notifyFormatter("Passed for: x < 2000", pb.Response_LOW)
	} else if res > 2000 && res < 4000 {
		return notifyFormatter("Passed for: 2000 <= x < 4000", pb.Response_MID)
	} else {
		// Check if gRPC server (gRPC_server) is running on a remote machine. Exit if not.
		remoteServerCmd := []string{"ps -ef | grep gRPC_server"}
		client := remote.PrepareSSHClient(user, hostname, password, 22)
		for i := range remoteServerCmd {
			out := remote.ConnectAndExecute(remoteServerCmd[i], client)
			if strings.Contains(out, "gRPC_server") == false {
				log.Printf("Server is not running on a remote machine: %s", address)
				os.Exit(101)
			} else {
				doGrpc()
				return notifyFormatter("Server found - see server console :) for: x >= 4000", pb.Response_HIGH)
			}
		}
		return notifyFormatter("Server NOT found :( and passed for: x >= 4000", pb.Response_HIGH)
	}
}

// Multiply two floats and generate a notification based on result.
func (s *server) DoSomething(ctx context.Context, inParam *pb.Request) (*pb.Response, error) {
	mulRes := inParam.A * inParam.B
	notification := notifyByResult(mulRes)

	// Return: name, result of multiplication and notification.
	return &pb.Response{
		Name:   inParam.Name,
		Result: inParam.A * inParam.B,
		//Rtype: resp_val,
		Notif: notification,
	}, nil
}

// Start gRPC server.
func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterTestServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
