package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"do_remote"

	pb "./protos"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	user        = "root"
	root        = "root"
	hostname    = "172.17.0.3"
	password    = "root"
	address     = "172.17.0.3:50051"
	defaultName = "grpc_client"
)

func main() {
	// Check if gRPC server (gRPC_client_server) is running on a remote machine. Exit if not.
	remoteServerCmd := []string{"ps -ef | grep gRPC_client_server"}
	client := remote.PrepareSSHClient(user, hostname, password, 22)
	for i := range remoteServerCmd {
		out := remote.ConnectAndExecute(remoteServerCmd[i], client)

		log.Println(out)

		if strings.Contains(out, "gRPC_client_server") == false {
			log.Printf("Server is not running on a remote machine: %s", address)
			os.Exit(101)
		}
	}

	// Set up a client connection to the given target (server).
	// Dial from grpc package.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	d := pb.NewTestServiceClient(conn)

	// Check program parameters.
	name := defaultName
	tabs := "\t\t\t"

	// Generate two random numbers of Float64 type. Repeat this 10000 times.
	for i := 0; i < 10000; i++ {
		a := rand.Float64() * 100
		b := rand.Float64() * 100

		// Call DoSomething function on a remote machine 10000 times and print out its response.
		// DoSomething will multiply two floats and generate a notification based on result.
		start := time.Now()
		resp, err := d.DoSomething(context.Background(), &pb.Request{Name: name, A: a, B: b})
		if err != nil {
			log.Fatalf("could not request: %v", err)
		}
		elapsed := time.Since(start) // 2016/11/09 16:38:56 Elapsed: 135.331µs - is that ok?

		// Print on every 1000-th call.
		if i%1000 == 0 {
			log.Println("=================")
			log.Printf("Call No: %d", i)
			log.Printf("Name: %s", resp.Name)
			log.Printf("Result: %.2f", resp.Result)
			log.Printf("Notif:\n%s%s\n%sValue: %s",
				tabs, resp.Notif[0].Notifstr, tabs, resp.Notif[0].Rtypenotif)
			log.Printf("Time elapsed: %s\n", elapsed)
		}
	}

	// Execute these commands in turn at the end.
	// Kill gRPC server (gRPC_client_server) first and ensure "vagrant halt" to be executed without errors.
	remoteServerCmd = []string{
		//"kill SIGTERM $(pidof gRPC_client_server)", "ps -ef | grep gRPC_client_server", "ls -l"}
		"ps -ef | grep gRPC_client_server", "ls -l"}

	// User, hostname, password, port.
	// Run kill command as root if the gRPC_client_server was started from Vagrantfile.
	client = remote.PrepareSSHClient(root, hostname, password, 22)

	for i := range remoteServerCmd {
		out := remote.ConnectAndExecute(remoteServerCmd[i], client)
		fmt.Println(out)
	}
}
