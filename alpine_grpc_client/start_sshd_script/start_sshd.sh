#!/bin/ash

# http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_12_01.html
# Generate host keys if not present.
ssh-keygen -A
#ssh-keygen -t rsa

# Do not detach (-D), log to stderr (-e), passthrough other arguments.
#exec /usr/sbin/sshd -D -e "$@"

# Just log to stderr (-e).
/usr/sbin/sshd -e

sleep 5

SSHD_PID=`cat /run/sshd.pid`
echo "To terminate ssh daemon:"
echo "kill -SIGTERM $SSHD_PID"

/bin/sh

