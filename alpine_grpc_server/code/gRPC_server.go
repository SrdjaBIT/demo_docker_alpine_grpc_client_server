package main

import (
	"log"
	"net"

	pb "./protos"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// Class server is used to implement test_service. TestService.
type server struct{}

// Format notification.
func notifyFormatter(notifStr string, notifType pb.Response_ResultType) []*pb.Response_Notification {
	return []*pb.Response_Notification{
		{
			Notifstr:   notifStr,
			Rtypenotif: notifType,
		},
	}
}

// Check results and generate a notification.
func notifyByResult(res float64) []*pb.Response_Notification {
	if res < 2000 {
		return notifyFormatter("Passed for: x < 2000", pb.Response_LOW)
	} else if res > 2000 && res < 4000 {
		return notifyFormatter("Passed for: 2000 <= x < 4000", pb.Response_MID)
	} else {
		return notifyFormatter("Passed for: x >= 4000", pb.Response_HIGH)
	}
}

// Multiply two floats and generate a notification based on result.
func (s *server) DoSomething(ctx context.Context, inParam *pb.Request) (*pb.Response, error) {
	mulRes := inParam.A * inParam.B
	notification := notifyByResult(mulRes)

	// Return: name, result of multiplication and notification.
	return &pb.Response{
		Name:   inParam.Name,
		Result: inParam.A * inParam.B,
		//Rtype: resp_val,
		Notif: notification,
	}, nil
}

// Start gRPC server.
func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterTestServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
