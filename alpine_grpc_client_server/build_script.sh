#!/bin/bash

client_image=$(docker images -q alpine_grpc_client_server)
none_image=$(docker images -f "dangling=true" -q)
golang_ver=`go version | grep -Po '(?<=go)[^ ]+'`

# Remove our generated client and intermediate images.
if [ $client_image ]; then
    echo -e "\e[1m\e[34mRemoving $client_image...\e[0m"
    docker rmi $client_image
fi

if [ $none_image ]; then
    echo -e "\e[1m\e[34mRemoving $none_image...\e[0m"
    docker rmi $none_image
fi

if [[ -z $client_image && -z $none_image ]]; then
    echo -e "\e[1m\e[34mNo images found...\e[0m"
fi

# Build a smallest possible Go static binary.
cd code
docker run --rm -it -v "$GOPATH":/gopath -v "$(pwd)":/app -e \
"GOPATH=/gopath" -w /app golang:$golang_ver sh -c 'CGO_ENABLED=0 go build -a \
--installsuffix cgo --ldflags="-s" -o gRPC_client_server.e'

cd ../

# Use Dockerfile in build process.
docker build -t alpine_grpc_client_server .

# Run container.
docker run --rm -ti alpine_grpc_client_server

